# symphonyQATask
This project is a simple UI automation project for a website, which runs tests with Chrome browser. It follows POM principle, therefore it has separated Page classes per each web page (all extending the BasePage).

For the tests I used TestNG and every test is annotated with @Test annotation. 
Currently there is just 1 test class containing 2 tests:

-companyPageTest

-careersPageTest

## Getting started
Install and configure JDK 1.8+

Install and configure Apache Maven 3.6.3+

Download the latest version of Intelij Idea Community Edition or any IDE of your choice

Make sure you have compatible version of Chrome browser and Chrome driver for it

## How to run it locally
Clone the project and import it in your IDE

Build the project with Maven to make sure all dependencies defined in pom.xml are downloaded

Go to src/test/java/tests/SymphonyWebsiteTests.java class and run the tests
