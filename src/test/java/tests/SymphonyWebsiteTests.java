package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.CareersPage;
import pages.CompanyPage;
import pages.HomePage;

public class SymphonyWebsiteTests {

  WebDriver chromeDriver;
  private HomePage homePage;
  private CompanyPage companyPage;
  private CareersPage careerPage;

  @BeforeMethod
  public void openAndMaximizeBrowser() {
    chromeDriver = new ChromeDriver();
    chromeDriver.get("https://symphony.is/");
    chromeDriver.manage().window().maximize();
    homePage = new HomePage(chromeDriver);
    companyPage = new CompanyPage(chromeDriver);
    careerPage = new CareersPage(chromeDriver);
  }

  @AfterMethod(alwaysRun = true)
  public void quitDriver() {
    chromeDriver.quit();
  }

  @Test(description = "test for company page content")
  public void companyPageTest() {
    homePage.hoverMenuLink(3);
    homePage.clickCompanyLink();
    //assert section text values
    Assert.assertEquals(companyPage.getSectionText(0), "HQ");
    Assert.assertEquals(companyPage.getSectionText(1), "Founded");
    Assert.assertEquals(companyPage.getSectionText(2), "Size");
    Assert.assertEquals(companyPage.getSectionText(3), "Consulting Locations");
    Assert.assertEquals(companyPage.getSectionText(4), "Engineering Hubs");
    Assert.assertEquals(companyPage.getSectionText(5), "Clients");
    Assert.assertEquals(companyPage.getSectionText(6), "Certifications");
    //assert url is correct
    Assert.assertEquals(chromeDriver.getCurrentUrl(), "https://symphony.is/about-us/company");
  }

  @Test(description = "test for careers page content")
  public void careersPageTest() {
    //open careers page
    homePage.clickMenuLink(4);
    careerPage.scrollDownToJobOpenings();
    //verify there are total 6 job openings for all locations
    Assert.assertEquals(careerPage.countJobOpenings(), 6);
    //write info for job openings in txt file
    careerPage.writeTitleAndLocation();
  }
}
