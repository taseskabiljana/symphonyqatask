package pages;

import java.time.Duration;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {
  public HomePage(WebDriver driver) {
    super(driver);
  }
  //about us element locator
  private By menuLinksLocator = By.cssSelector("a.header--nav-link");
  //company link locator
  private By companyLocator = By.xpath("//a[contains(text(),'Company')]");

  public void hoverMenuLink(int position) {
    List<WebElement> menuLinks = driver.findElements(menuLinksLocator);
    action.moveToElement(menuLinks.get(position)).perform();
  }

  public void clickMenuLink(int position) {
    List<WebElement> menuLinks = driver.findElements(menuLinksLocator);
    menuLinks.get(position).click();
  }

  public void clickCompanyLink() {
    wait.until(ExpectedConditions.visibilityOfElementLocated(companyLocator));
    WebElement companyLink = driver.findElement(companyLocator);
    companyLink.click();
  }
}
