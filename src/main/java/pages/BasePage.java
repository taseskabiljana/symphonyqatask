package pages;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
  WebDriver driver;
  WebDriverWait wait;
  Actions action;

  BasePage(WebDriver driver) {
    this.driver = driver;
    wait = new WebDriverWait(driver, Duration.ofSeconds(5));
    action = new Actions(driver);
  }

  public void writeToTextFile(String info) {
    File file = new File("src\\main\\resources\\JobOpeningsInfo.txt");
    try {
      FileWriter fw = new FileWriter(file, true);
      //write info to file
      fw.write(info);
      fw.flush();
      fw.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}