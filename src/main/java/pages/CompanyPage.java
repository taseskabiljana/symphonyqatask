package pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CompanyPage extends BasePage {
  public CompanyPage(WebDriver driver) {
    super(driver);
  }
  //section text locator
  private By sectionTextLocator = By.cssSelector(".pageMetaDetails--list li>strong");

  public String getSectionText(int position) {
    wait.until(ExpectedConditions.visibilityOfElementLocated(sectionTextLocator));
    List<WebElement> sectionTextValues = driver.findElements(sectionTextLocator);
    return sectionTextValues.get(position).getText();
  }
}
