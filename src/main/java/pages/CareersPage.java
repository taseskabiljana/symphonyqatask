package pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CareersPage extends BasePage{
  public CareersPage(WebDriver driver) {
   super(driver);
  }

  //job openings
  private By jobOpeningsListSelector = By.cssSelector(".currentOpenings--jobs");
  private By jobOpeningsSelector = By.cssSelector(".currentOpenings--job");

  public int countJobOpenings() {
    List<WebElement> jobOpenings = driver.findElements(jobOpeningsSelector);
    return jobOpenings.size();
  }

  public void scrollDownToJobOpenings(){
    wait.until(ExpectedConditions.visibilityOfElementLocated(jobOpeningsListSelector));
    WebElement jobOpeningsList = driver.findElement(jobOpeningsListSelector);
    action.moveToElement(jobOpeningsList);
  }

  public void writeTitleAndLocation() {
    List<WebElement> jobTitle = driver.findElements(By.cssSelector(".currentOpenings--job-title"));
    List<WebElement> jobLocation = driver.findElements(By.cssSelector(".currentOpenings--job-locationWrapper-name"));
    for(int i=0;i<jobTitle.size();i++) {
      writeToTextFile(jobTitle.get(i).getText()+", ");
      writeToTextFile(jobLocation.get(i).getText());
      writeToTextFile("\n");
    }
  }
}
